# Geekfactory GDPServer Library for Arduino #

The GDPServer library is designed to allow embedded devices based on Arduino to be discovered by PCs, tablets and cellphones that
implement the GDP protocol. This library was developed by www.geekfactory.mx for use on our products or prototypes, so the name
stands for __G__eek __D__iscovery __P__rotocol.

The protocol itself is very basic, it uses UDP to send and receive messages on the network and one of the goals is to keep it simple
in order to avoid excesive resource usage. The protocol operates in the following manner:

* The PC willing to discover devices on the network sends a broadcast message using UDP with the destination port set to 30303
* The GDP enabled device(s) or embedded devices on the network receive the request and process it
* The embedded prepares a reply message containing it´s MAC address, it´s name and the type of IP assignation method (static or DHCP).
* The embedded device sends a unicast message to the IP that generated the original request message
* The PC receives the unicast message and parses the MAC addres, name and other fields
* The PC knows the IP address of the embedded device by inspecting the IP packet header (source address) of the UDP datagram

## Basic library usage ##

The following example is the basic implementation of a GDP enabled device. Source code is commented for easy understanding.

```cpp
/**
   GeekFactory - "INNOVATING TOGETHER"
   www.geekfactory.mx

   Ejemplo para implementar un servidor GDP (Geek Discovery Protocol) con la libreria de arduino GDPServer.
   Example that implements a GDP Server (Geek Discovery Protocol) using GPDServer arduino library .
*/
#include <Ethernet.h>
#include <GDPServer.h>

// MAC address used for ethernet
uint8_t mac[] = {0x60, 0x45, 0xCB, 0x9A, 0x47, 0xAA};
// UDP object for Geek Discovery Protocol
EthernetUDP udp;
// Geek Discovery Protocol server object
GDPServer gdp(udp);

void setup() {
  // Start serial to print debug messages
  Serial.begin(115200);
  Serial.println(F("GDPServer Demo"));
  
  // Begin ethernet, get address by DHCP
  if (!Ethernet.begin(mac)) {
    Serial.println(F("Could not obtain IP address"));
    for (;;);
  }
  Serial.println(F("Ethernet OK"));

  // Start discovery protocol server
  // Pass the MAC address, device name, address type and device type
  gdp.begin(mac, F("GDP-Device-0001"), E_GDP_ADDR_DHCP, 0xAA);
  // Send power up broadcast
  gdp.announce();
}

void loop() {
  // Process discovery messages
  gdp.process();
}
```

## Project objectives ##

* Create a reference implementation of the GDP protocol on Arduino
* Minimize the resource usage of the implementation
* Implement the GDP protocol for ethernet and wifi embedded devices
* Document the first version of the protocol and publish the specification
* Implement a client library for PCs in the java programming language
* Create a demo application for a PC

## Supported devices ##

The library has been tested on the following hardware:

* Arduino UNO + Ethernet Shield
* Arduino MEGA + Ethernet Shield
* Arduino MEGA + Adafruit WINC1500 WiFi Shield


## Contact me ##

* Feel free to write for any inquiry: ruben at geekfactory.mx 
* Check our website: https://www.geekfactory.mx

