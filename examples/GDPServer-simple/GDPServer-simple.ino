/**
   GeekFactory - "INNOVATING TOGETHER"
   www.geekfactory.mx

   Ejemplo para implementar un servidor GDP (Geek Discovery Protocol) con la libreria de arduino GDPServer.
   Example that implements a GDP Server (Geek Discovery Protocol) using GPDServer arduino library .
*/
#include <Ethernet.h>
#include <GDPServer.h>

// MAC address used for ethernet
uint8_t mac[] = {0x60, 0x45, 0xCB, 0x9A, 0x47, 0xAA};
// UDP object for Geek Discovery Protocol
EthernetUDP udp;
// Geek Discovery Protocol server object
GDPServer gdp(udp);

void setup() {
  // Start serial to print debug messages
  Serial.begin(1152000);
  Serial.println(F("GDPServer Demo"));
  
  // Begin ethernet, get address by DHCP
  if (!Ethernet.begin(mac)) {
    Serial.println(F("Could not obtain IP address"));
    for (;;);
  }
  Serial.println(F("Ethernet OK"));

  // Start discovery protocol server
  // Pass the MAC address, device name, address type and device type
  gdp.begin(mac, F("GDP-Device-0001"), E_GDP_ADDR_DHCP, 0xAA);
  // Send power up broadcast
  gdp.announce();
}

void loop() {
  // Process discovery messages
  gdp.process();
}

