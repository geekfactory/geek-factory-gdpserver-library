#ifndef GDPSERVER_H
#define GDPSERVER_H
/*-------------------------------------------------------------*
 *		Includes and dependencies			*
 *-------------------------------------------------------------*/
#include "Arduino.h"
#include "Udp.h"

/*-------------------------------------------------------------*
 *		Library Configuration				*
 *-------------------------------------------------------------*/
/**
 * Defines the library version string
 */
#define GDPSERVER_VERSION_STRING	"1.0.0"

/**
 * Uncomment this to enable debug messages
 */
#define CONFIG_GDPSERVER_DEBUG

/**
 * Configures the stream object used to send debug messages
 */
#define CONFIG_GDPSERVER_DEBUGSTREAM	Serial1

/**
 * Configures the UDP port to receive discovery broadcast messages
 */
#define CONFIG_GDPSERVER_LISTEN_PORT	30303	

/*-------------------------------------------------------------*
 *		Macros & definitions				*
 *-------------------------------------------------------------*/

/*-------------------------------------------------------------*
 *		Typedefs enums & structs			*
 *-------------------------------------------------------------*/

/**
 * Enumeration that defines the states for the Geek Discovery Protocol Service
 */
enum gdpserver_states {
	E_GDPSERVER_HOME,
	E_GDPSERVER_LISTEN,
	E_GDPSERVER_REPLY,
};

/**
 * Enumeration that defines the operation codes for GDP messages
 */
enum gdp_opcodes {
	E_GDP_DISCOVERY_REQUEST = 0x44,
	E_GDP_DISCOVERY_REPLY = 0x52,
	E_GDP_UNSOLICITED_UPDATE = 0x55,
};

/**
 * Enumeration that defines
 */
enum gdp_address_type {
	E_GDP_ADDR_STATIC = 0x01,
	E_GDP_ADDR_DHCP = 0x02,
};

/**
 * Structure representing the message header for Geek Discovery Protocol messages
 */
struct GDPHeader {
	uint8_t version;
	uint8_t opcode;
	uint8_t device_type;
	uint8_t size;
};

/*-------------------------------------------------------------*
 *		Class declaration				*
 *-------------------------------------------------------------*/
class GDPServer {
public:
	/**
	 * Creates an instance of GDPServer that is used to receive and
	 * process device discovery messages. This class is the implementation
	 * of the Geek Discovery Protocol.
	 * 
	 * @param udp An UDP instance to send or receive datagrams
	 * @param port The port to listen for Discovery Request Messages
	 */
	GDPServer(UDP & udp, int port = CONFIG_GDPSERVER_LISTEN_PORT);
	
	/**
	 * Sets the network address to send on discovery messages, the device name
	 * and the address type (static or DHCP). Optionally you can specify the
	 * type of device that is responding to discovery messages.
	 * 
	 * @param mac The MAC address of the network interface
	 * @param name The device´s friendly name (max 15 chars) as in netbios
	 * @param addressType The type of IP address assignation of this device
	 * according to gdp_address_type enum.
	 * @param deviceType Optional parameter to specify the device type
         */
	bool begin(const uint8_t * mac, const char * name, enum gdp_address_type addressType, uint8_t deviceType = 0xFF);

	/**
	 * Sets the network address to send on discovery messages, the device name
	 * and the address type (static or DHCP). Optionally you can specify the
	 * type of device that is responding to discovery messages.
	 * 
	 * @param mac The MAC address of the network interface
	 * @param name The device´s friendly name (max 15 chars) as in netbios
	 * @param addressType The type of IP address assignation of this device
	 * according to gdp_address_type enum.
	 * @param deviceType Optional parameter to specify the device type
         */
	bool begin(const uint8_t * mac, const __FlashStringHelper * name, enum gdp_address_type addressType, uint8_t deviceType = 0XFF);

	/**
	 * Main processing of the GDPServer happens inside this method.
	 * Should be called constantly from the main program loop.
	 */
	void process();

	/**
	 * Broadcasts a message indicating a power up event or DHCP update.
	 * 
	 * This method is meant to be called at power up or when a DHCP lease
	 * is renewed or rebound to automatically announce the presence of the
	 * device to other nodes.
	 */
	void announce();
private:
	/**
	 * Writes the device name (16 characters, including null terminator) to
	 * the UDP datagram.
	 */
	void writeName();
	
	/**
	 * Initializes the UDP object to listen / send
	 */
	bool initialize();
	
	UDP & _udp;
	uint16_t _port;
	const uint8_t * _mac = 0;
	const char * _rname = 0;
	PGM_P _fname;
	enum gdp_address_type _addressType;
	uint8_t _deviceType;
	
	enum gdpserver_states _gdpstate = E_GDPSERVER_HOME;
};

#endif