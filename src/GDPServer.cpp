#include "GDPServer.h"

/*-------------------------------------------------------------*
 *		Macros for debugging				*
 *-------------------------------------------------------------*/
#ifdef CONFIG_GDPSERVER_DEBUG
#define DEBUG_PRINT(...)	CONFIG_GDPSERVER_DEBUGSTREAM.print(__VA_ARGS__)
#define DEBUG_PRINTLN(...)	CONFIG_GDPSERVER_DEBUGSTREAM.println(__VA_ARGS__)
#else
#define DEBUG_PRINT(...)
#define DEBUG_PRINTLN(...)
#endif

/*-------------------------------------------------------------*
 *		Class implementation				*
 *-------------------------------------------------------------*/
GDPServer::GDPServer(UDP & udp, int port) : _udp(udp), _port(port)
{
}

bool GDPServer::begin(const uint8_t * mac, const char * name, enum gdp_address_type addressType, uint8_t deviceType)
{
	_mac = mac;
	_rname = name;
	_addressType = addressType;
	_deviceType = deviceType;
	return initialize();
}

bool GDPServer::begin(const uint8_t * mac, const __FlashStringHelper * name, enum gdp_address_type addressType, uint8_t deviceType)
{
	_mac = mac;
	_fname = reinterpret_cast<PGM_P> (name);
	_addressType = addressType;
	_deviceType = deviceType;
	return initialize();
}

void GDPServer::process()
{
	int size = 0;
	struct GDPHeader header;

	switch (_gdpstate) {
	case E_GDPSERVER_HOME:
		// Do nothing, we don,t have UDP communication or server is paused
		break;

	case E_GDPSERVER_LISTEN:
		// Wait for request packet
		size = _udp.parsePacket();
		// Return if no packet is available
		if (size == 0)
			return;
		// Process the received packet
		DEBUG_PRINTLN(F("\tGDPServer,process(),received request"));
		if (size >= 4) {
			_udp.read((char *) &header, sizeof(struct GDPHeader));
			if (header.version == 1 && header.opcode == E_GDP_DISCOVERY_REQUEST) {
				DEBUG_PRINTLN(F("\tGDPServer,process(),valid"));
				// Prepare for reply transmission
				_gdpstate = E_GDPSERVER_REPLY;
				break;
			}
		}
		DEBUG_PRINTLN(F("\tGDPServer,process(),invalid"));
		break;

	case E_GDPSERVER_REPLY:
		// Transmit response message as unicast
#ifdef CONFIG_GDPSERVER_DEBUG
		DEBUG_PRINT(F("\tGDPServer,process(),reply to "));
		_udp.remoteIP().printTo(CONFIG_GDPSERVER_DEBUGSTREAM);
		DEBUG_PRINT(F(":"));
		DEBUG_PRINTLN(_udp.remotePort());
#endif
		// Fill response header
		header.version = 1;
		header.opcode = E_GDP_DISCOVERY_REPLY;
		header.device_type = _deviceType;
		header.size = 23;
		// Send UDP reply to sender
		_udp.beginPacket(_udp.remoteIP(), _udp.remotePort());
		// Send the header
		_udp.write((const uint8_t *) &header, sizeof(struct GDPHeader));
		// Send response body
		_udp.write(_addressType);
		_udp.write(_mac, 6);
		writeName();
		// End UDP datagram
		_udp.endPacket();
		_gdpstate = E_GDPSERVER_LISTEN;
		break;
	}
}

void GDPServer::announce()
{
	// Structure to hold message header
	struct GDPHeader header;
	// Check if we can send UDP datagrams
	if (_gdpstate != E_GDPSERVER_HOME) {
		DEBUG_PRINTLN(F("\tGDPServer,announce(),send broadcast"));
		// Fill header with valid information
		header.version = 1;
		header.opcode = E_GDP_UNSOLICITED_UPDATE;
		header.device_type = _deviceType;
		header.size = 23;
		// Broadcast address as destination
		IPAddress broadcastAddress(255, 255, 255, 255);
		// Send UDP broadcast
		_udp.beginPacket(broadcastAddress, _port + 1);
		// Send message header
		_udp.write((const uint8_t *) &header, sizeof(struct GDPHeader));
		// Send response body
		_udp.write(_addressType);
		_udp.write(_mac, 6);
		writeName();
		// End UDP transmission
		_udp.endPacket();
	}
}

void GDPServer::writeName()
{
	int len = 0;
	int count = 0;
	char c = 0;

	// Send exactly 16 bytes from RAM or flash
	if (_rname) {
		len = strlen(_rname);
		count = _udp.write((const uint8_t *) _rname, min(15, len));
		for (int i = 0; i < (16 - count); i++)
			_udp.write(0x00);

	} else {
		PGM_P pgmptr = _fname;
		len = strlen_P(pgmptr);
		do {
			c = pgm_read_byte(pgmptr++);
			_udp.write(c);
			count++;
		} while (c && count < 15 && count < len);
		for (int i = 0; i < (16 - count); i++)
			_udp.write(0x00);
	}
}

bool GDPServer::initialize()
{
	// Start debug messages
	DEBUG_PRINTLN(F("\tGDPServer,initialize(),init"));
	// Check if we can use an UDP socket
	if (_udp.begin(_port) == 1) {
		_gdpstate = E_GDPSERVER_LISTEN;
		DEBUG_PRINTLN(F("\tGDPServer,initialize(),UDP ok"));
		return true;
	} else {
		_gdpstate = E_GDPSERVER_HOME;
		DEBUG_PRINTLN(F("\tGDPServer,initialize(),DNP nok"));
		return false;
	}
}